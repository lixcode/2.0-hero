#ifndef il_InterLevel
#define il_InterLevel

namespace icL {

namespace memory {
class Memory;
}

namespace il {

class CP;
class VMLayer;
class VMStack;
class FrontEnd;
class SourceServer;
class StatefulServer;

/**
 * @brief The InterLevel struct contains interfaces to each level
 */
struct InterLevel
{
    ~InterLevel() {}

    /// \brief mem is a pointer to memory level
    memory::Memory * mem = nullptr;

    /// \brief cpu is a pointer to command processor level
    CP * cpu = nullptr;

    /// \brief vm is a pointer to virtual machine layer
    VMLayer * vm = nullptr;

    /// \brief vms is a pointer to highest level - virtual machine stack
    VMStack * vms = nullptr;

    /// \brief source is a pointer to the source server
    SourceServer * source = nullptr;

    /// \brief stateful is a pointer to the setateful server
    StatefulServer * stateful = nullptr;
};

}  // namespace il
}  // namespace icL


#endif  // il_InterLevel
