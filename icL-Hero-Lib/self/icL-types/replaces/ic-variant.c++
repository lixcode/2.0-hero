#include "ic-variant.h++"

#include <icL-types/json/js-array.h++>
#include <icL-types/json/js-object.h++>
#include <icL-types/replaces/ic-datetime.h++>
#include <icL-types/replaces/ic-regex.h++>

#include <icL-il/structures/cookie-data.h++>
#include <icL-il/structures/db-target.h++>
#include <icL-il/structures/element.h++>
#include <icL-il/structures/file-target.h++>
#include <icL-il/structures/lambda-target.h++>

#include <icL-memory/structures/packed-value-item.h++>
#include <icL-memory/structures/set.h++>

#include <cassert>



icVariant::icVariant() {
    m_type = icType::Initial;
}

icVariant::icVariant(const bool value) {
    d      = value;
    m_type = icType::Bool;
}

icVariant::icVariant(const int & value)
    : icVariant() {
    d      = value;
    m_type = icType::Int;
}

icVariant::icVariant(const double & value)
    : icVariant() {
    d      = value;
    m_type = icType::Double;
}

icVariant::icVariant(const icString & string)
    : icVariant() {
    d      = string;
    m_type = icType::String;
}

icVariant::icVariant(const icDateTime & dt)
    : icVariant() {
    d      = dt;
    m_type = icType::DateTime;
}

icVariant::icVariant(const icStringList & list)
    : icVariant() {
    d      = list;
    m_type = icType::StringList;
}

icVariant::icVariant(const icRegEx & re)
    : icVariant() {
    d      = re;
    m_type = icType::RegEx;
}

icVariant::icVariant(const icL::memory::PackedValue & packet)
    : icVariant() {
    d      = packet;
    m_type = icType::Packed;
}

icVariant::icVariant(const jsArray & array)
    : icVariant() {
    d      = array;
    m_type = icType::JsArray;
}

icVariant::icVariant(const jsObject & object)
    : icVariant() {
    d      = object;
    m_type = icType::JsObject;
}

icVariant icVariant::makeVoid() {
    icVariant ret;
    ret.m_type = icType::Void;
    return ret;
}

icType icVariant::type() const {
    return m_type;
}

bool icVariant::isBool() const {
    return m_type == icType::Bool;
}

bool icVariant::isInt() const {
    return m_type == icType::Int;
}

bool icVariant::isDouble() const {
    return m_type == icType::Double;
}

bool icVariant::isString() const {
    return m_type == icType::String;
}

bool icVariant::isDateTime() const {
    return m_type == icType::DateTime;
}

bool icVariant::isStringList() const {
    return m_type == icType::StringList;
}

bool icVariant::isRegEx() const {
    return m_type == icType::RegEx;
}

bool icVariant::isObject() const {
    return m_type == icType::Object;
}

bool icVariant::isJsObject() const {
    return m_type == icType::JsObject;
}

bool icVariant::isSet() const {
    return m_type == icType::Set;
}

bool icVariant::isPacked() const {
    return m_type == icType::Packed;
}

bool icVariant::isJsArray() const {
    return m_type == icType::JsArray;
}

bool icVariant::isPosition() const {
    return m_type == icType::Position;
}

bool icVariant::isNull() const {
    return m_type == icType::Initial;
}

bool icVariant::isVoid() const {
    return m_type == icType::Void;
}

bool icVariant::isValid() const {
    return m_type != icType::Void && m_type != icType::Initial;
}

template <typename T>
icVariant icVariant::fromValueTemplate(const T & value, icType type) {
    icVariant ret;
    ret.d      = value;
    ret.m_type = type;
    return ret;
}

icVariant icVariant::fromValue(const bool & value) {
    return fromValueTemplate(value, icType::Bool);
}

icVariant icVariant::fromValue(const int & value) {
    return fromValueTemplate(value, icType::Int);
}

icVariant icVariant::fromValue(const double & value) {
    return fromValueTemplate(value, icType::Double);
}

icVariant icVariant::fromValue(const icString & string) {
    return fromValueTemplate(string, icType::String);
}

icVariant icVariant::fromValue(const icDateTime & dt) {
    return fromValueTemplate(dt, icType::DateTime);
}

icVariant icVariant::fromValue(const icStringList & list) {
    return fromValueTemplate(list, icType::StringList);
}

icVariant icVariant::fromValue(const icRegEx & re) {
    return fromValueTemplate(re, icType::RegEx);
}

icVariant icVariant::fromValue(const icL::memory::Object & obj) {
    return fromValueTemplate(obj, icType::Object);
}

icVariant icVariant::fromValue(const icL::memory::Set & set) {
    return fromValueTemplate(set, icType::Set);
}

icVariant icVariant::fromValue(const icL::memory::PackedValue & packet) {
    return fromValueTemplate(packet, icType::Packed);
}

icVariant icVariant::fromValue(const jsArray & array) {
    return fromValueTemplate(array, icType::JsArray);
}

icVariant icVariant::fromValue(const jsObject & object) {
    return fromValueTemplate(object, icType::JsObject);
}

icVariant icVariant::fromValue(const icL::il::Position & postion) {
    return fromValueTemplate(postion, icType::Position);
}

const bool & icVariant::toBool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}

const int & icVariant::toInt() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

const double & icVariant::toDouble() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

const icString & icVariant::toString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

const icDateTime & icVariant::toDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

const icStringList & icVariant::toStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

const icRegEx & icVariant::toRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

const icL::memory::Object & icVariant::toObject() const {
    assert(isObject());
    return std::any_cast<const icL::memory::Object &>(d);
}

const icL::memory::Set & icVariant::toSet() const {
    assert(isSet());
    return std::any_cast<const icL::memory::Set &>(d);
}

const icL::memory::PackedValue & icVariant::toPacked() const {
    assert(isPacked());
    return std::any_cast<const icL::memory::PackedValue &>(d);
}

const jsArray & icVariant::toArray() const {
    assert(isJsArray());
    return std::any_cast<const jsArray &>(d);
}

const jsObject & icVariant::toJsObject() const {
    assert(isJsObject());
    return std::any_cast<const jsObject &>(d);
}

const icL::il::Position & icVariant::toPosition() const {
    assert(isPosition());
    return std::any_cast<const icL::il::Position &>(d);
}

icVariant & icVariant::clear() {
    m_type = icType::Void;
    return *this;
}

bool icVariant::operator==(const icVariant & other) const {
    if (m_type != other.m_type)
        return false;

    switch (m_type) {
    case icType::Initial:
    case icType::Void:
        return true;

    case icType::Bool:
        return toBool() == other.toBool();

    case icType::Int:
        return toInt() == other.toInt();

    case icType::Double:
        return toDouble() == other.toDouble();

    case icType::String:
        return toString() == other.toString();

    case icType::DateTime:
        return toDateTime() == other.toDateTime();

    case icType::StringList:
        return toStringList() == other.toStringList();

    case icType::RegEx:
        return toRegEx() == other.toRegEx();

    case icType::Object:
        return toObject() == other.toObject();

    case icType::Set:
        return toSet() == other.toSet();

    case icType::Packed:
        return toPacked() == other.toPacked();

    case icType::JsArray:
        return toArray() == other.toArray();

    case icType::JsObject:
        return toJsObject() == other.toJsObject();

    case icType::Position:
        return toPosition().absolute == other.toPosition().absolute;
    }
}

icVariant::operator icRegEx() const {
    assert(isRegEx());
    return std::any_cast<const icRegEx &>(d);
}

icVariant::operator icStringList() const {
    assert(isStringList());
    return std::any_cast<const icStringList &>(d);
}

icVariant::operator icDateTime() const {
    assert(isDateTime());
    return std::any_cast<const icDateTime &>(d);
}

icVariant::operator icString() const {
    assert(isString());
    return std::any_cast<const icString &>(d);
}

icVariant::operator double() const {
    assert(isDouble());
    return std::any_cast<const double &>(d);
}

icVariant::operator int() const {
    assert(isInt());
    return std::any_cast<const int &>(d);
}

icVariant::operator bool() const {
    assert(isBool());
    return std::any_cast<const bool &>(d);
}
