#ifndef type_TypesEnum
#define type_TypesEnum

enum class icType {
    Initial,
    Void,
    Bool,
    Int,
    Double,
    String,
    DateTime,
    StringList,
    RegEx,
    Object,
    Set,
    JsArray,
    JsObject,
    Position,
    Packed
};

#endif  // type_TypesEnum
