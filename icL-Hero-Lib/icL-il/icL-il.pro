QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17 no_include_pwd
QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    self \
    ../icL-Pro/src \
    ../icL-Pro/src/icL-il/auxiliary \
    ../icL-Pro/src/icL-il/main \
    ../icL-Pro/src/icL-il/structures

SOURCES += \ \
    ../icL-Pro/src/icL-il/structures/code-fragment.c++ \
    ../icL-Pro/src/icL-il/structures/cookie-data.c++ \
    ../icL-Pro/src/icL-il/structures/db-target.c++ \
    ../icL-Pro/src/icL-il/structures/element.c++ \
    ../icL-Pro/src/icL-il/structures/file-target.c++ \
    ../icL-Pro/src/icL-il/structures/lambda-target.c++ \
    ../icL-Pro/src/icL-il/structures/listen-handler.c++ \
    ../icL-Pro/src/icL-il/structures/mouse-data.c++ \
    ../icL-Pro/src/icL-il/structures/position.c++ \
    ../icL-Pro/src/icL-il/structures/signal.c++ \
    ../icL-Pro/src/icL-il/structures/target-data.c++

HEADERS += \ \
    ../icL-Pro/src/icL-il/auxiliary/source-of-code.h++ \
    ../icL-Pro/src/icL-il/main/ajax-handler.h++ \
    ../icL-Pro/src/icL-il/main/ce.h++ \
    ../icL-Pro/src/icL-il/main/cp.h++ \
    ../icL-Pro/src/icL-il/main/db-server.h++ \
    ../icL-Pro/src/icL-il/main/enums.h++ \
    ../icL-Pro/src/icL-il/main/file-server.h++ \
    ../icL-Pro/src/icL-il/main/frontend.h++ \
    ../icL-Pro/src/icL-il/main/interlevel.h++ \
    ../icL-Pro/src/icL-il/main/listen.h++ \
    ../icL-Pro/src/icL-il/main/node.h++ \
    ../icL-Pro/src/icL-il/main/request.h++ \
    ../icL-Pro/src/icL-il/main/source-server.h++ \
    ../icL-Pro/src/icL-il/main/stateful-server.h++ \
    ../icL-Pro/src/icL-il/main/vmlayer.h++ \
    ../icL-Pro/src/icL-il/main/vmstack.h++ \
    ../icL-Pro/src/icL-il/structures/code-fragment.h++ \
    ../icL-Pro/src/icL-il/structures/cookie-data.h++ \
    ../icL-Pro/src/icL-il/structures/db-target.h++ \
    ../icL-Pro/src/icL-il/structures/element.h++ \
    ../icL-Pro/src/icL-il/structures/file-target.h++ \
    ../icL-Pro/src/icL-il/structures/lambda-target.h++ \
    ../icL-Pro/src/icL-il/structures/listen-handler.h++ \
    ../icL-Pro/src/icL-il/structures/mouse-data.h++ \
    ../icL-Pro/src/icL-il/structures/position.h++ \
    ../icL-Pro/src/icL-il/structures/return.h++ \
    ../icL-Pro/src/icL-il/structures/signal.h++ \
    ../icL-Pro/src/icL-il/structures/steptype.h++ \
    ../icL-Pro/src/icL-il/structures/target-data.h++

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
