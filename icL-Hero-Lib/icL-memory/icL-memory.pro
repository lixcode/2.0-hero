QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17 no_include_pwd
QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    self \
    ../icL-Pro/src \
    ../icL-Pro/src/icL-memory/state \
    ../icL-Pro/src/icL-memory/structures

SOURCES += \ \
    ../icL-Pro/src/icL-memory/state/datacontainer.c++ \
    ../icL-Pro/src/icL-memory/state/memory.c++ \
    ../icL-Pro/src/icL-memory/state/stackcontainer.c++ \
    ../icL-Pro/src/icL-memory/state/statecontainer.c++ \
    ../icL-Pro/src/icL-memory/structures/argument.c++ \
    ../icL-Pro/src/icL-memory/structures/functioncontainer.c++ \
    ../icL-Pro/src/icL-memory/structures/packed-value-item.c++ \
    ../icL-Pro/src/icL-memory/structures/parameter.c++ \
    ../icL-Pro/src/icL-memory/structures/set.c++ \
    ../icL-Pro/src/icL-memory/structures/type.c++

HEADERS += \ \
    ../icL-Pro/src/icL-memory/state/datacontainer.h++ \
    ../icL-Pro/src/icL-memory/state/memory.h++ \
    ../icL-Pro/src/icL-memory/state/signals.h++ \
    ../icL-Pro/src/icL-memory/state/stackcontainer.h++ \
    ../icL-Pro/src/icL-memory/state/statecontainer.h++ \
    ../icL-Pro/src/icL-memory/structures/arg-value.h++ \
    ../icL-Pro/src/icL-memory/structures/argument.h++ \
    ../icL-Pro/src/icL-memory/structures/function-call.h++ \
    ../icL-Pro/src/icL-memory/structures/function.h++ \
    ../icL-Pro/src/icL-memory/structures/functioncontainer.h++ \
    ../icL-Pro/src/icL-memory/structures/packed-value-item.h++ \
    ../icL-Pro/src/icL-memory/structures/parameter.h++ \
    ../icL-Pro/src/icL-memory/structures/set.h++ \
    ../icL-Pro/src/icL-memory/structures/type.h++

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
