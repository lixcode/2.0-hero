QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17 no_include_pwd
QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    self \
    ../icL-Pro/src \
    ../icL-Pro/src/icL-cp/cp \
    ../icL-Pro/src/icL-cp/flyer \
    ../icL-Pro/src/icL-cp/source

SOURCES += \
    ../icL-Pro/src/icL-cp/cp/cp.c++ \
    ../icL-Pro/src/icL-cp/flyer/flyer.c++ \
    ../icL-Pro/src/icL-cp/flyer/icL.c++ \
    ../icL-Pro/src/icL-cp/flyer/js.c++ \
    ../icL-Pro/src/icL-cp/source/source-of-code.c++ \
    ../icL-Pro/src/icL-cp/source/source-server.c++

HEADERS += \
    ../icL-Pro/src/icL-cp/cp/cp.h++ \
    ../icL-Pro/src/icL-cp/flyer/flyer.h++ \
    ../icL-Pro/src/icL-cp/flyer/i-flyer.h++ \
    ../icL-Pro/src/icL-cp/flyer/icL.h++ \
    ../icL-Pro/src/icL-cp/flyer/js.h++ \
    ../icL-Pro/src/icL-cp/result/js-type.h++ \
    ../icL-Pro/src/icL-cp/result/structs.h++ \
    ../icL-Pro/src/icL-cp/result/type.h++ \
    ../icL-Pro/src/icL-cp/source/source-of-code.h++ \
    ../icL-Pro/src/icL-cp/source/source-server.h++

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
