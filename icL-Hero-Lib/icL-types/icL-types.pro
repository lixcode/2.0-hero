QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17 no_include_pwd
QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    self \
    ../icL-Pro/src \
    ../icL-Pro/src/icL-types/json \
    ../icL-Pro/src/icL-types/replaces \
    ../icL-Pro/src/icL-types/global

SOURCES += \ \
    ../icL-Pro/src/icL-types/json/js-array.c++ \
    ../icL-Pro/src/icL-types/json/js-document.c++ \
    ../icL-Pro/src/icL-types/json/js-object.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-char.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-datetime.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-debug.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-file.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-hash-functions.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-image.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-list.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-math.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-multi.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-object.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-pair.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-rect.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-regex.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-set.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-string-list.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-string.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-text-stream.c++ \
    ../icL-Pro/src/icL-types/replaces/ic-variant.c++

HEADERS += \ \
    ../icL-Pro/src/icL-types/global/global.h++ \
    ../icL-Pro/src/icL-types/global/icl-types.h++ \
    ../icL-Pro/src/icL-types/json/js-array.h++ \
    ../icL-Pro/src/icL-types/json/js-document.h++ \
    ../icL-Pro/src/icL-types/json/js-object.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-char.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-datetime.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-debug.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-file.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-hash-functions.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-image.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-list.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-math.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-multi.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-object.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-pair.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-rect.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-regex.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-set.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-string-list.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-string.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-text-stream.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-types-enum.h++ \
    ../icL-Pro/src/icL-types/replaces/ic-variant.h++

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
