QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17 no_include_pwd
QMAKE_CXXFLAGS += -std=c++17

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    self \
    ../icL-Pro/src \
    ../icL-Pro/src/icL-shared/main \
    ../icL-Pro/src/icL-shared/mocks

SOURCES += \ \
    ../icL-Pro/src/icL-shared/main/engine.c++ \
    ../icL-Pro/src/icL-shared/main/stateful-server.c++ \
    ../icL-Pro/src/icL-shared/mocks/frontend.c++ \
    ../icL-Pro/src/icL-shared/mocks/request.c++

HEADERS += \ \
    ../icL-Pro/src/icL-shared/main/engine.h++ \
    ../icL-Pro/src/icL-shared/main/stateful-server.h++ \
    ../icL-Pro/src/icL-shared/mocks/frontend.h++ \
    ../icL-Pro/src/icL-shared/mocks/request.h++

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
